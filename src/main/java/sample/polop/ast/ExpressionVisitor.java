package sample.polop.ast;

public interface ExpressionVisitor<T> {
    T visitArrayLiteral(final ArrayLiteral expression);

    T visitAssign(final Assign expression);

    T visitBinaryOperation(final BinaryOperation expression);

    T visitBoolean(final BooleanLiteral expression);

    T visitCall(final Call expression);

    T visitConcat(final Concat expression);

    T visitCondition(final Condition expression);

    T visitDictLiteral(final DictLiteral expression);

    T visitFunctor(final Functor expression);

    T visitIndex(final Index expression);

    T visitNumber(final NumberLiteral expression);

    T visitReturn(final Return expression);

    T visitSequence(final Sequence expression);

    T visitStringLiteral(final StringLiteral expression);

    T visitTupleLiteral(final TupleLiteral expression);

    T visitUnaryOperation(final UnaryOperation expression);

    T visitUndefinedLiteral(final UndefinedLiteral expression);

    T visitVariable(final Variable expression);

    T visitWhile(final While expression);

    T visitFor(final For expression);

    T visitForeach(final Foreach expression);
}
