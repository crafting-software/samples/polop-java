package sample.polop.ast;

public final class Variable implements Expression {
    private final String name;

    public String name() {
        return name;
    }

    Variable(final String name) {
        this.name = name;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitVariable(this);
    }
}
