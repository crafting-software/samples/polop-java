package sample.polop.ast.analyse;

import com.google.common.collect.Sets;
import sample.polop.ast.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Sets.difference;
import static java.util.Set.copyOf;

public class GatherVariableReference implements ExpressionVisitor<Set<String>> {

    public static Set<String> referencesOf(final Expression expression, final Collection<String> ignored) {
        return difference(expression.accept(new GatherVariableReference()), copyOf(ignored));
    }

    public static Set<String> referencesOf(final Expression expression) {
        return referencesOf(expression, Set.of());
    }

    public Set<String> visitArrayLiteral(final ArrayLiteral expression) {
        return Set.of();
    }

    public Set<String> visitAssign(final Assign expression) {
        return referencesOf(expression.value());
    }

    public Set<String> visitBinaryOperation(final BinaryOperation expression) {
        return union(expression.left(), expression.right());
    }

    public Set<String> visitBoolean(final BooleanLiteral expression) {
        return Set.of();
    }

    public Set<String> visitCall(final Call expression) {
        return Sets.union(referencesOf(expression.functor()), union(expression.arguments()));
    }

    public Set<String> visitConcat(final Concat expression) {
        return union(expression.elements());
    }

    public Set<String> visitCondition(final Condition expression) {
        return union(expression.condition(), expression.whenTrue(), expression.whenFalse());
    }

    public Set<String> visitDictLiteral(final DictLiteral expression) {
        return Set.of();
    }

    public Set<String> visitFunctor(final Functor expression) {
        return referencesOf(expression.body(), expression.parameters());
    }

    public Set<String> visitIndex(final Index expression) {
        return union(expression.index(), expression.value());
    }

    public Set<String> visitNumber(final NumberLiteral expression) {
        return Set.of();
    }

    public Set<String> visitReturn(final Return expression) {
        return referencesOf(expression.value());
    }

    public Set<String> visitSequence(final Sequence expression) {
        return union(expression.children());
    }

    public Set<String> visitStringLiteral(final StringLiteral expression) {
        return Set.of();
    }

    public Set<String> visitTupleLiteral(final TupleLiteral expression) {
        return Set.of();
    }

    public Set<String> visitUnaryOperation(final UnaryOperation expression) {
        return referencesOf(expression.child());
    }

    public Set<String> visitUndefinedLiteral(final UndefinedLiteral expression) {
        return Set.of();
    }

    public Set<String> visitVariable(final Variable expression) {
        return Set.of(expression.name());
    }

    public Set<String> visitWhile(final While expression) {
        return union(expression.condition(), expression.body());
    }

    public Set<String> visitFor(final For expression) {
        return union(expression.from(), expression.to(), expression.body());
    }

    public Set<String> visitForeach(final Foreach expression) {
        return union(expression.collection(), expression.body());
    }

    private Set<String> union(final Expression... expression) {
        return Arrays.stream(expression).map(GatherVariableReference::referencesOf).reduce(Sets::union).orElse(Set.of());
    }

    private Set<String> union(final List<Expression> expressions) {
        return expressions.stream().map(GatherVariableReference::referencesOf).reduce(Sets::union).orElse(Set.of());
    }
}
