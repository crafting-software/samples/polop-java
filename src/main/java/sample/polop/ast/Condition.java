package sample.polop.ast;

public final class Condition implements Expression {
    private final Expression condition;
    private final Expression whenTrue;
    private final Expression whenFalse;

    Condition(final Expression condition, final Expression whenTrue, final Expression whenFalse) {
        this.condition = condition;
        this.whenTrue = whenTrue;
        this.whenFalse = whenFalse;
    }

    public Expression condition() {
        return condition;
    }

    public Expression whenTrue() {
        return whenTrue;
    }

    public Expression whenFalse() {
        return whenFalse;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitCondition(this);
    }
}
