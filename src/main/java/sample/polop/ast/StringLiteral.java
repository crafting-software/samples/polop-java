package sample.polop.ast;

public final class StringLiteral implements Expression {
    private final String value;

    StringLiteral(final String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitStringLiteral(this);
    }
}
