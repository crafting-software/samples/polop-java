package sample.polop.ast;

public final class For implements Expression {
    private final String index;
    private final Expression from;
    private final Expression to;
    private final Expression body;

    For(final String index, final Expression from, final Expression to, final Expression body) {
        this.index = index;
        this.from = from;
        this.to = to;
        this.body = body;
    }


    public String index() {
        return index;
    }

    public Expression from() {
        return from;
    }

    public Expression to() {
        return to;
    }

    public Expression body() {
        return body;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitFor(this);
    }
}
