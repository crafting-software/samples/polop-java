package sample.polop.ast;

public final class Assign implements Expression {
    private final String name;
    private final Expression value;

    Assign(final String name, final Expression value) {
        this.name = name;
        this.value = value;
    }

    public Expression value() {
        return value;
    }

    public String name() {
        return name;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitAssign(this);
    }
}
