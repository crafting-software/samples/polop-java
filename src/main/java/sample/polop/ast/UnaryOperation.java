package sample.polop.ast;

public final class UnaryOperation implements Expression {
    private final UnaryOperator operator;
    private final Expression child;

    UnaryOperation(final UnaryOperator operator, final Expression child) {
        this.operator = operator;
        this.child = child;
    }

    public UnaryOperator operator() {
        return operator;
    }

    public Expression child() {
        return child;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitUnaryOperation(this);
    }
}
