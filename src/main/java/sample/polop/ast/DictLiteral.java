package sample.polop.ast;

import java.util.Map;

import static java.util.Collections.unmodifiableMap;

public final class DictLiteral implements Expression {
    private final Map<Expression, Expression> values;

    DictLiteral(final Map<Expression, Expression> values) {
        this.values = values;
    }

    public Map<Expression, Expression> values() {
        return unmodifiableMap(values);
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitDictLiteral(this);
    }
}
