package sample.polop.ast;

public final class While implements Expression {
    private final Expression condition;
    private final Expression body;

    public While(Expression condition, Expression body) {
        this.condition = condition;
        this.body = body;
    }


    public Expression condition() {
        return condition;
    }

    public Expression body() {
        return body;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitWhile(this);
    }
}
