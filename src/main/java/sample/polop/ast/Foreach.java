package sample.polop.ast;

public class Foreach implements Expression {
    private final String index;
    private final Expression collection;
    private final Expression body;

    Foreach(final String index, final Expression collection, final Expression body) {
        this.index = index;
        this.collection = collection;
        this.body = body;
    }

    public String index() {
        return index;
    }

    public Expression collection() {
        return collection;
    }

    public Expression body() {
        return body;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitForeach(this);
    }
}
