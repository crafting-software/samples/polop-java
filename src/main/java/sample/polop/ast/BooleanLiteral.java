package sample.polop.ast;

public final class BooleanLiteral implements Expression {
    private final boolean value;

    BooleanLiteral(final boolean value) {
        this.value = value;
    }

    public boolean value() {
        return value;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitBoolean(this);
    }
}
