package sample.polop.ast;

public enum BinaryOperator {
    ADD,
    AND,
    OR,
    XOR,
    SUB,
    MUL,
    DIV,
    MOD,
    EQ,
    NEQ,
    LE,
    LT,
    GE,
    GT,
    MAX,
    MIN
}
