package sample.polop.ast;

public enum UnaryOperator {
    NEG,
    NOT,
    ABS,
    SQR,
    SQRT,
    LEN,
    STR,
    BOOL
}
