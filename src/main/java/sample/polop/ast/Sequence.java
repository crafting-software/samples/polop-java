package sample.polop.ast;

import java.util.List;

public final class Sequence implements Expression {
    private final List<Expression> children;

    Sequence(final List<Expression> children) {
        this.children = children;
    }

    public List<Expression> children() {
        return children;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitSequence(this);
    }
}
