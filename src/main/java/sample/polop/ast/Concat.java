package sample.polop.ast;

import java.util.Collections;
import java.util.List;

public final class Concat implements Expression {
    private final List<Expression> elements;

    Concat(final List<Expression> elements) {
        this.elements = elements;
    }

    public List<Expression> elements() {
        return Collections.unmodifiableList(elements);
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitConcat(this);
    }
}
