package sample.polop.ast;

import java.util.Collections;
import java.util.List;

public final class Functor implements Expression {
    private final List<String> parameters;
    private final Expression body;

    Functor(final List<String> parameters, final Expression body) {
        this.parameters = parameters;
        this.body = body;
    }

    public Expression body() {
        return body;
    }

    public List<String> parameters() {
        return Collections.unmodifiableList(parameters);
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitFunctor(this);
    }
}
