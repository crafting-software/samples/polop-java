package sample.polop.ast;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

public final class Expressions {
    private static final UndefinedLiteral UNDEFINED_LITERAL = new UndefinedLiteral();

    public static Expression abs(final Expression child) {
        return new UnaryOperation(UnaryOperator.ABS, child);
    }

    public static Expression add(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.ADD, left, right);
    }

    public static Expression and(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.AND, left, right);
    }

    public static Expression assign(final String name, final Expression value) {
        return new Assign(name, value);
    }

    public static Expression array(final Expression... elements) {
        return new ArrayLiteral(Lists.newArrayList(elements));
    }

    public static Expression bool(final Expression child) {
        return new UnaryOperation(UnaryOperator.BOOL, child);
    }

    public static Expression call(final Expression functor, final Expression... arguments) {
        return new Call(functor, Lists.newArrayList(arguments));
    }

    public static Expression cond(final Expression condition, final Expression whenTrue) {
        return cond(condition, whenTrue, undefined());
    }

    public static Expression cond(final Expression condition, final Expression whenTrue, final Expression whenFalse) {
        return new Condition(condition, whenTrue, whenFalse);
    }

    public static Expression concat(final Expression... elements) {
        return new Concat(Lists.newArrayList(elements));
    }

    public static Map.Entry<Expression, Expression> entry(final Expression key, final Expression value) {
        return Maps.immutableEntry(key, value);
    }

    public static Expression dict(final Map.Entry<Expression, Expression>... entries) {
        return new DictLiteral(ImmutableMap.<Expression, Expression>builder().putAll(asList(entries)).build());
    }

    public static Expression div(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.DIV, left, right);
    }

    public static Expression eq(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.EQ, left, right);
    }

    public static Expression functor(final String parameters, final Expression body) {
        return functor(asList(parameters.split("\\s+")), body);
    }

    public static Expression functor(final List<String> parameters, final Expression body) {
        return new Functor(parameters, body);
    }

    public static Expression ge(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.GE, left, right);
    }

    public static Expression gt(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.GT, left, right);
    }

    public static Expression index(final Expression value, final Expression index) {
        return new Index(value, index);
    }

    public static Expression le(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.LE, left, right);
    }

    public static Expression literal(final boolean value) {
        return new BooleanLiteral(value);
    }

    public static Expression literal(final long value) {
        return new NumberLiteral(BigDecimal.valueOf(value));
    }

    public static Expression literal(final BigDecimal value) {
        return new NumberLiteral(value);
    }

    public static Expression literal(final double value) {
        return new NumberLiteral(BigDecimal.valueOf(value));
    }

    public static Expression literal(final String value) {
        return new StringLiteral(value);
    }

    public static Expression lt(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.LT, left, right);
    }

    public static Expression max(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.MAX, left, right);
    }

    public static Expression min(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.MIN, left, right);
    }

    public static Expression mod(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.MOD, left, right);
    }

    public static Expression mul(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.MUL, left, right);
    }

    public static Expression neg(final Expression child) {
        return new UnaryOperation(UnaryOperator.NEG, child);
    }

    public static Expression neq(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.NEQ, left, right);
    }

    public static Expression not(final Expression child) {
        return new UnaryOperation(UnaryOperator.NOT, child);
    }

    public static Expression or(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.OR, left, right);
    }

    public static Expression ret(final Expression value) {
        return new Return(value);
    }

    public static Expression sequence(final Expression... elements) {
        return new Sequence(Lists.newArrayList(elements));
    }

    public static Expression sqr(final Expression child) {
        return new UnaryOperation(UnaryOperator.SQR, child);
    }

    public static Expression sqrt(final Expression child) {
        return new UnaryOperation(UnaryOperator.SQRT, child);
    }

    public static Expression str(final Expression child) {
        return new UnaryOperation(UnaryOperator.STR, child);
    }

    public static Expression sub(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.SUB, left, right);
    }

    public static Expression tuple(final Expression... elements) {
        return new TupleLiteral(Lists.newArrayList(elements));
    }

    public static Expression undefined() {
        return UNDEFINED_LITERAL;
    }

    public static Expression variable(final String name) {
        return new Variable(name);
    }

    public static Expression xor(final Expression left, final Expression right) {
        return new BinaryOperation(BinaryOperator.XOR, left, right);
    }

    public static Expression while_(final Expression condition, final Expression body) {
        return new While(condition, body);
    }

    public static Expression for_(final String index, final Expression from, final Expression to, final Expression body) {
        return new For(index, from, to, body);
    }

    public static Expression foreach(final String index, final Expression collection, final Expression body) {
        return new Foreach(index, collection, body);
    }
}
