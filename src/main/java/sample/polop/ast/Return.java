package sample.polop.ast;

public final class Return implements Expression {
    private final Expression value;

    Return(final Expression value) {
        this.value = value;
    }

    public Expression value() {
        return value;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitReturn(this);
    }
}
