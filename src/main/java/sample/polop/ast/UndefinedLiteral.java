package sample.polop.ast;

public final class UndefinedLiteral implements Expression {
    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitUndefinedLiteral(this);
    }
}
