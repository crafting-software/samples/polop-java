package sample.polop.ast;

import java.util.Collections;
import java.util.List;

public final class Call implements Expression {
    private final Expression functor;
    private final List<Expression> arguments;

    Call(final Expression functor, final List<Expression> arguments) {
        this.functor = functor;
        this.arguments = arguments;
    }

    public Expression functor() {
        return functor;
    }

    public List<Expression> arguments() {
        return Collections.unmodifiableList(arguments);
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitCall(this);
    }
}
