package sample.polop.ast;

import java.util.List;

import static java.util.Collections.unmodifiableList;

public final class ArrayLiteral implements Expression {
    private final List<Expression> elements;

    ArrayLiteral(final List<Expression> elements) {
        this.elements = elements;
    }

    public List<Expression> elements() {
        return unmodifiableList(elements);
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitArrayLiteral(this);
    }
}
