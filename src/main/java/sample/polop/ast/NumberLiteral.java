package sample.polop.ast;

import java.math.BigDecimal;

public final class NumberLiteral implements Expression {
    private final BigDecimal value;

    NumberLiteral(final BigDecimal value) {
        this.value = value;
    }

    public BigDecimal value() {
        return value;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitNumber(this);
    }
}
