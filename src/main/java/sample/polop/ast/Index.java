package sample.polop.ast;

public final class Index implements Expression {
    private final Expression value;
    private final Expression index;

    Index(final Expression value, final Expression index) {
        this.value = value;
        this.index = index;
    }

    public Expression index() {
        return index;
    }

    public Expression value() {
        return value;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitIndex(this);
    }
}
