package sample.polop.ast;

public interface Expression {
    <T> T accept(final ExpressionVisitor<T> visitor);
}
