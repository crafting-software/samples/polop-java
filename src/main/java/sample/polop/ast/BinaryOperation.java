package sample.polop.ast;

public final class BinaryOperation implements Expression {
    private final BinaryOperator operator;
    private final Expression left;
    private final Expression right;

    BinaryOperation(final BinaryOperator operator, final Expression left, final Expression right) {
        this.operator = operator;
        this.left = left;
        this.right = right;
    }

    public BinaryOperator operator() {
        return operator;
    }

    public Expression left() {
        return left;
    }

    public Expression right() {
        return right;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitBinaryOperation(this);
    }
}
