package sample.polop.interpreter;

import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Map;

import static sample.polop.interpreter.Values.lazyOf;
import static sample.polop.interpreter.Values.undefined;

final class Frame {
    private final Map<String, LazyValue> values;

    static Frame empty() {
        return new Frame(new HashMap<>());
    }

    private Frame(final Map<String, LazyValue> values) {
        this.values = values;
    }

    LazyValue get(final String name) {
        return values.getOrDefault(name, lazyOf(undefined()));
    }

    Frame set(final String name, final LazyValue value) {
        values.put(name, value);
        return this;
    }

    Frame duplicate() {
        return new Frame(Maps.newHashMap(values));
    }

    Frame closure(final Iterable<String> names) {
        final Frame closure = empty();
        for (final String name : names) {
            if (values.containsKey(name)) {
                closure.set(name, values.get(name));
            }
        }
        return closure;
    }
}
