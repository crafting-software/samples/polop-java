package sample.polop.interpreter;

import java.util.List;
import java.util.stream.Collectors;

public final class ArrayValue implements Value {
    private final List<Value> values;

    ArrayValue(final List<Value> values) {
        this.values = values;
    }

    public List<Value> values() {
        return values;
    }

    public String repr() {
        return str();
    }

    public String str() {
        return "[" + values.stream().map(Value::repr).collect(Collectors.joining(", ")) + "]";
    }

    public BooleanValue asBoolean() {
        return new BooleanValue(!values.isEmpty());
    }

    public NumberValue asNumber() {
        throw new RuntimeException("bad type");
    }

    public FunctorValue asFunctor() {
        throw new RuntimeException("bad type");
    }

    public Value index(final Value index) {
        return values.get(index.asNumber().value().intValue());
    }
}
