package sample.polop.interpreter;

import java.math.BigDecimal;

public final class NumberValue implements Value {
    private final BigDecimal value;

    NumberValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal value() {
        return value;
    }

    public String repr() {
        return str();
    }

    public String str() {
        return value.toString();
    }

    public BooleanValue asBoolean() {
        return new BooleanValue(value.compareTo(BigDecimal.ZERO) != 0);
    }

    public NumberValue asNumber() {
        return this;
    }

    public FunctorValue asFunctor() {
        throw new RuntimeException("bad type");
    }

    public Value index(final Value index) {
        throw new RuntimeException("bad type");
    }
}
