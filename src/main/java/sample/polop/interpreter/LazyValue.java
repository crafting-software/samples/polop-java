package sample.polop.interpreter;

public interface LazyValue {
    Value get();
}
