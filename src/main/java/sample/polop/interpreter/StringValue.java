package sample.polop.interpreter;

import static java.lang.String.valueOf;

public final class StringValue implements Value {
    private final String value;

    StringValue(final String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    public String repr() {
        return "'" + value + "'";
    }

    public String str() {
        return value;
    }

    public BooleanValue asBoolean() {
        return new BooleanValue(!value.isEmpty());
    }

    public NumberValue asNumber() {
        throw new RuntimeException("bad type");
    }

    public FunctorValue asFunctor() {
        throw new RuntimeException("bad type");
    }

    public Value index(final Value index) {
        return Values.value(valueOf(value.charAt(index.asNumber().value().intValue())));
    }
}
