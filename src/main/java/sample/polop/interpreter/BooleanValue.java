package sample.polop.interpreter;

public final class BooleanValue implements Value {
    private final boolean value;

    BooleanValue(final boolean value) {
        this.value = value;
    }

    public boolean value() {
        return value;
    }

    public String repr() {
        return str();
    }

    public String str() {
        return Boolean.toString(value);
    }

    public BooleanValue asBoolean() {
        return this;
    }

    public NumberValue asNumber() {
        throw new RuntimeException("bad type");
    }

    public FunctorValue asFunctor() {
        throw new RuntimeException("bad type");
    }

    public Value index(final Value index) {
        throw new RuntimeException("bad type");
    }
}
