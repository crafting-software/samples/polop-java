package sample.polop.interpreter;

import com.google.common.collect.Maps;
import sample.polop.ast.*;

import java.math.BigDecimal;
import java.util.Map;

import static java.math.MathContext.DECIMAL128;
import static java.util.stream.Collectors.toList;
import static sample.polop.ast.analyse.GatherVariableReference.referencesOf;
import static sample.polop.interpreter.Values.*;

public final class Interpreter implements ExpressionVisitor<Value> {
    private final Frame context;

    public static Value interpret(final Expression expression) {
        return interpret(expression, Frame.empty());
    }

    public static Value interpret(final Expression expression, final Frame context) {
        return expression.accept(new Interpreter(context));
    }

    public Interpreter(final Frame context) {
        this.context = context;
    }

    public Value visitArrayLiteral(final ArrayLiteral expression) {
        return array(expression.elements().stream().map(this::eval).collect(toList()));
    }

    public Value visitBoolean(final BooleanLiteral expression) {
        return value(expression.value());
    }

    public Value visitCall(final Call expression) {
        final FunctorValue functor = eval(expression.functor()).asFunctor();
        final Frame newContext = functor.context(expression.arguments().stream().map(this::lazy).collect(toList()));
        return functor.body().accept(new Interpreter(newContext));
    }

    public Value visitConcat(final Concat expression) {
        return value(expression.elements().stream().map(e -> eval(e).str()).reduce(String::concat).orElse(""));
    }

    public Value visitCondition(final Condition expression) {
        if (evalAsBoolean(expression.condition())) {
            return eval(expression.whenTrue());
        }
        return eval(expression.whenFalse());
    }

    public Value visitDictLiteral(final DictLiteral expression) {
        final Map<String, Value> values = Maps.newHashMap();
        for (final Map.Entry<Expression, Expression> entry : expression.values().entrySet()) {
            values.put(eval(entry.getKey()).repr(), eval(entry.getValue()));
        }
        return dict(values);
    }

    public Value visitFunctor(final Functor expression) {
        return functor(expression, context.closure(referencesOf(expression.body(), expression.parameters())));
    }

    public Value visitIndex(final Index expression) {
        return eval(expression.value()).index(eval(expression.index()));
    }

    public Value visitNumber(final NumberLiteral expression) {
        return value(expression.value());
    }

    public Value visitReturn(final Return expression) {
        return undefined();
    }

    public Value visitSequence(final Sequence expression) {
        return expression.children().stream().map(this::eval).reduce((x, y) -> y).orElse(undefined());
    }

    public Value visitStringLiteral(final StringLiteral expression) {
        return value(expression.value());
    }

    public Value visitTupleLiteral(final TupleLiteral expression) {
        return tuple(expression.elements().stream().map(this::eval).collect(toList()));
    }

    public Value visitAssign(final Assign expression) {
        final Value value = eval(expression.value());
        context.set(expression.name(), lazyOf(value));
        return value;
    }

    public Value visitBinaryOperation(final BinaryOperation expression) {
        switch (expression.operator()) {
            case ADD:
                return value(evalAsNumber(expression.left()).add(evalAsNumber(expression.right())));
            case AND:
                if (evalAsBoolean(expression.left())) {
                    return eval(expression.right()).asBoolean();
                }
                return value(false);
            case OR:
                if (evalAsBoolean(expression.left())) {
                    return value(true);
                }
                return eval(expression.right()).asBoolean();
            case XOR:
                return value(evalAsBoolean(expression.left()) ^ evalAsBoolean(expression.right()));
            case SUB:
                return value(evalAsNumber(expression.left()).subtract(evalAsNumber(expression.right())));
            case MUL:
                return value(evalAsNumber(expression.left()).multiply(evalAsNumber(expression.right())));
            case DIV:
                return value(evalAsNumber(expression.left()).divide(evalAsNumber(expression.right()), DECIMAL128));
            case MOD:
                return value(evalAsNumber(expression.left()).remainder(evalAsNumber(expression.right())));
            case EQ:
                return value(eval(expression.left()).repr().equals(eval(expression.right()).repr()));
            case NEQ:
                return value(!eval(expression.left()).repr().equals(eval(expression.right()).repr()));
            case LE:
                return value(evalAsNumber(expression.left()).compareTo(evalAsNumber(expression.right())) <= 0);
            case LT:
                return value(evalAsNumber(expression.left()).compareTo(evalAsNumber(expression.right())) < 0);
            case GE:
                return value(evalAsNumber(expression.left()).compareTo(evalAsNumber(expression.right())) >= 0);
            case GT:
                return value(evalAsNumber(expression.left()).compareTo(evalAsNumber(expression.right())) > 0);
            case MAX:
                return value(evalAsNumber(expression.left()).max(evalAsNumber(expression.right())));
            case MIN:
                return value(evalAsNumber(expression.left()).min(evalAsNumber(expression.right())));
        }
        return undefined();
    }


    public Value visitUnaryOperation(final UnaryOperation expression) {
        final Value child = eval(expression.child());
        switch (expression.operator()) {
            case NEG:
                return value(child.asNumber().value().negate());
            case NOT:
                return value(!child.asBoolean().value());
            case ABS:
                return value(child.asNumber().value().abs());
            case SQR:
                return value(child.asNumber().value().multiply(child.asNumber().value()));
            case SQRT:
                return value(child.asNumber().value().sqrt(DECIMAL128));
            case LEN:
                break;
            case STR:
                return value(child.str());
            case BOOL:
                return child.asBoolean();
        }
        return undefined();
    }

    public Value visitUndefinedLiteral(final UndefinedLiteral expression) {
        return undefined();
    }

    public Value visitVariable(final Variable expression) {
        return context.get(expression.name()).get();
    }

    public Value visitWhile(final While expression) {
        Value result = undefined();
        while (eval(expression.condition()).asBoolean().value()) {
            result = eval(expression.body());
        }
        return result;
    }

    public Value visitFor(final For expression) {
        return undefined();
    }

    public Value visitForeach(final Foreach expression) {
        return undefined();
    }

    private Value eval(final Expression expression) {
        return expression.accept(this);
    }

    private BigDecimal evalAsNumber(final Expression expression) {
        return eval(expression).asNumber().value();
    }

    private boolean evalAsBoolean(final Expression expression) {
        return eval(expression).asBoolean().value();
    }

    private LazyValue lazy(final Expression expression) {
        return () -> eval(expression);
    }
}
