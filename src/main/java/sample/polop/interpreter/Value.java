package sample.polop.interpreter;


public interface Value {
    String repr();

    String str();

    BooleanValue asBoolean();

    NumberValue asNumber();

    FunctorValue asFunctor();

    Value index(Value index);
}
