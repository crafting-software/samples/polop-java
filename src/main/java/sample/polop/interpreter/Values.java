package sample.polop.interpreter;

import sample.polop.ast.Functor;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

final class Values {

    private static final UndefinedValue UNDEFINED_VALUE = new UndefinedValue();

    static Value array(final Value... values) {
        return array(asList(values));
    }

    static Value array(final List<Value> values) {
        return new ArrayValue(values);
    }

    static Value tuple(final Value... values) {
        return tuple(asList(values));
    }

    static Value tuple(final List<Value> values) {
        return new TupleValue(values);
    }

    static Value value(final boolean value) {
        return new BooleanValue(value);
    }

    static Value value(final long value) {
        return new NumberValue(BigDecimal.valueOf(value));
    }

    static Value value(final double value) {
        return new NumberValue(BigDecimal.valueOf(value));
    }

    static Value value(final BigDecimal value) {
        return new NumberValue(value);
    }

    static Value value(final String value) {
        return new StringValue(value);
    }

    static Value undefined() {
        return UNDEFINED_VALUE;
    }

    static Value dict(final Map<String, Value> values) {
        return new DictValue(values);
    }

    static Value functor(final Functor functor, final Frame context) {
        return new FunctorValue(functor, context);
    }

    static LazyValue lazyOf(final Value value) {
        return () -> value;
    }
}
