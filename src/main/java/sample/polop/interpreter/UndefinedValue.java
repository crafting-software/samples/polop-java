package sample.polop.interpreter;

public final class UndefinedValue implements Value {
    public String repr() {
        return str();
    }

    public String str() {
        return "<undefined>";
    }

    public BooleanValue asBoolean() {
        return new BooleanValue(false);
    }

    public NumberValue asNumber() {
        throw new RuntimeException("bad type");
    }

    public FunctorValue asFunctor() {
        throw new RuntimeException("bad type");
    }

    public Value index(final Value index) {
        throw new RuntimeException("bad type");
    }
}
