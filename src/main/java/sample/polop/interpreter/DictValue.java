package sample.polop.interpreter;

import com.google.common.collect.Ordering;

import java.util.Map;

import static java.util.stream.Collectors.joining;

public final class DictValue implements Value {
    private final Map<String, Value> values;

    DictValue(final Map<String, Value> values) {
        this.values = values;
    }

    public Map<String, Value> values() {
        return values;
    }

    public String repr() {
        return str();
    }

    public String str() {
        return "{" + Ordering.natural().sortedCopy(values.keySet()).stream().map(k -> k + ": " + values.get(k).repr()).collect(joining(", ")) + "}";
    }

    public BooleanValue asBoolean() {
        return new BooleanValue(!values.isEmpty());
    }

    public NumberValue asNumber() {
        throw new RuntimeException("bad type");
    }

    public FunctorValue asFunctor() {
        throw new RuntimeException("bad type");
    }

    public Value index(final Value index) {
        return values.get(index.repr());
    }
}
