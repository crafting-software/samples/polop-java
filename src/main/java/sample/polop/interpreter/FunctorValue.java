package sample.polop.interpreter;

import com.google.common.collect.Streams;
import io.vavr.Tuple;
import sample.polop.ast.Expression;
import sample.polop.ast.Functor;

import java.util.List;

public final class FunctorValue implements Value {
    private final Functor functor;
    private final Frame context;

    FunctorValue(final Functor functor, final Frame context) {
        this.functor = functor;
        this.context = context;
    }

    public Expression body() {
        return functor.body();
    }

    public Frame context(final List<LazyValue> arguments) {
        final Frame result = context.duplicate();
        Streams.zip(functor.parameters().stream(), arguments.stream(), Tuple::of).forEach(t -> result.set(t._1, t._2));
        return result;
    }

    public String repr() {
        return str();
    }

    public String str() {
        return "functor";
    }

    public BooleanValue asBoolean() {
        throw new RuntimeException("bad type");
    }

    public NumberValue asNumber() {
        throw new RuntimeException("bad type");
    }

    public FunctorValue asFunctor() {
        return this;
    }

    public Value index(final Value index) {
        throw new RuntimeException("bad type");
    }
}
