package sample.polop.ast.analyse;

import com.google.common.base.Joiner;
import com.google.common.collect.Ordering;
import org.junit.jupiter.api.Test;
import sample.polop.ast.Expression;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.polop.ast.Expressions.*;
import static sample.polop.ast.analyse.GatherVariableReference.referencesOf;

class GatherVariableReferenceTest {
    @Test
    void literals_should_gather_no_variables() {
        assertEquals("", variablesOf(literal(true)));
        assertEquals("", variablesOf(literal(42)));
        assertEquals("", variablesOf(literal("polop")));
        assertEquals("", variablesOf(undefined()));
    }

    @Test
    void literals_should_gather_variable() {
        assertEquals("x", variablesOf(variable("x")));
    }

    @Test
    void literals_should_gather_unary_operation() {
        assertEquals("x", variablesOf(neg(variable("x"))));
    }

    @Test
    void literals_should_gather_binary_operation() {
        assertEquals("x, y", variablesOf(add(variable("x"), variable("y"))));
    }

    @Test
    void literals_should_gather_sequence() {
        assertEquals("x, y", variablesOf(sequence(variable("x"), variable("y"))));
    }

    @Test
    void literals_should_gather_concat() {
        assertEquals("x, y", variablesOf(concat(variable("x"), variable("y"))));
    }

    @Test
    void literals_should_gather_condition() {
        assertEquals("c, x, y", variablesOf(cond(variable("c"), variable("x"), variable("y"))));
    }

    @Test
    void literals_should_gather_call() {
        assertEquals("f, x, y", variablesOf(call(variable("f"), variable("x"), variable("y"))));
    }

    @Test
    void literals_should_gather_functor() {
        assertEquals("y", variablesOf(functor("x", add(variable("x"), variable("y")))));
    }


    private String variablesOf(final Expression expression) {
        return Joiner.on(", ").join(Ordering.natural().sortedCopy(referencesOf(expression)));
    }
}