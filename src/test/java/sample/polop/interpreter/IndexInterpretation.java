package sample.polop.interpreter;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.polop.ast.Expressions.*;
import static sample.polop.interpreter.DSL.evaluate;

class IndexInterpretation {

    @Test
    void should_evaluate_an_index_on_tuple() {
        assertEquals("3", evaluate(index(tuple(literal(3), literal(6)), literal(0))));
        assertEquals("6", evaluate(index(tuple(literal(3), literal(6)), literal(1))));
    }

    @Test
    void should_evaluate_an_index_on_array() {
        assertEquals("3", evaluate(index(array(literal(3), literal(6)), literal(0))));
        assertEquals("6", evaluate(index(array(literal(3), literal(6)), literal(1))));
    }

    @Test
    void should_evaluate_an_index_on_string() {
        assertEquals("'p'", evaluate(index(literal("polop"), literal(0))));
        assertEquals("'o'", evaluate(index(literal("polop"), literal(1))));
    }

    @Test
    void should_evaluate_an_index_on_dict() {
        assertEquals("42", evaluate(index(dict(entry(literal("x"), literal(42))), literal("x"))));
    }
}
