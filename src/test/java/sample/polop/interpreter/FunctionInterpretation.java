package sample.polop.interpreter;

import org.junit.jupiter.api.Test;
import sample.polop.ast.Expression;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.polop.ast.Expressions.*;
import static sample.polop.interpreter.DSL.evaluate;

class FunctionInterpretation {

    @Test
    void should_evaluate_function_call() {
        assertEquals("42",
                evaluate(call(functor("x", add(variable("x"), literal(1))), literal(41))));
    }

    @Test
    void should_evaluate_function_by_name() {
        assertEquals("42",
                evaluate(
                        sequence(
                                assign("inc", functor("x", add(variable("x"), literal(1)))),
                                call(variable("inc"), literal(41)))));
    }

    @Test
    void should_evaluate_high_order_function() {
        final Expression addition =
                functor("x",
                        functor("y",
                                add(variable("x"), variable("y"))));

        assertEquals("42",
                evaluate(call(call(addition, literal(2)), literal(40))));
    }
}
