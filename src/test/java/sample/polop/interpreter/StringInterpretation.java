package sample.polop.interpreter;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.polop.ast.Expressions.*;
import static sample.polop.interpreter.DSL.evaluate;

class StringInterpretation {

    @Test
    void should_evaluate_string_cast() {
        assertEquals("'true'", evaluate(str(literal(true))));
        assertEquals("'false'", evaluate(str(literal(false))));
        assertEquals("'42'", evaluate(str(literal(42))));
    }

    @Test
    void should_evaluate_concatenation() {
        assertEquals("'HelloWorld'", evaluate(concat(literal("Hello"), literal("World"))));
    }
}
