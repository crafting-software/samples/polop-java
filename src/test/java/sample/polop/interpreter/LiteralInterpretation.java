package sample.polop.interpreter;

import io.vavr.test.Property;
import org.junit.jupiter.api.Test;

import static io.vavr.test.Arbitrary.integer;
import static io.vavr.test.Arbitrary.string;
import static io.vavr.test.Gen.choose;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.polop.ast.Expressions.*;
import static sample.polop.interpreter.DSL.areEquals;
import static sample.polop.interpreter.DSL.evaluate;

class LiteralInterpretation {

    @Test
    void should_evaluate_an_undefined_literal() {
        assertEquals("<undefined>", evaluate(undefined()));
    }

    @Test
    void should_evaluate_an_number_literal() {
        Property.def("Number literal evaluation")
                .forAll(integer())
                .suchThat(value -> areEquals(evaluate(literal(value)), value))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_a_boolean_literal() {
        assertEquals("true", evaluate(literal(true)));
        assertEquals("false", evaluate(literal(false)));
    }

    @Test
    void should_evaluate_a_string_literal() {
        Property.def("String literal evaluation")
                .forAll(string(choose('A', 'z')))
                .suchThat(value -> areEquals(evaluate(literal(value)), "'" + value + "'"))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_an_array_literal() {
        assertEquals("[3, 7]", evaluate(array(literal(3), literal(7))));
    }

    @Test
    void should_evaluate_an_tuple_literal() {
        assertEquals("(3, 7)", evaluate(tuple(literal(3), literal(7))));
    }

    @Test
    void should_evaluate_an_dict_literal() {
        assertEquals("{'x': 7, 'y': 9}", evaluate(dict(entry(literal("x"), literal(7)), entry(literal("y"), literal(9)))));
    }
}
