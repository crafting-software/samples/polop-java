package sample.polop.interpreter;

import io.vavr.test.Property;
import org.junit.jupiter.api.Test;

import static io.vavr.test.Arbitrary.integer;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.polop.ast.Expressions.*;
import static sample.polop.interpreter.DSL.areEquals;
import static sample.polop.interpreter.DSL.evaluate;

class ComparisonInterpretation {

    @Test
    void should_evaluate_equalilty() {
        assertEquals("true", evaluate(eq(literal(true), literal(true))));
        assertEquals("true", evaluate(eq(literal(false), literal(false))));
        assertEquals("true", evaluate(eq(literal(42), literal(42))));
        assertEquals("true", evaluate(eq(literal("polop"), literal("polop"))));

        assertEquals("false", evaluate(eq(literal(true), literal(false))));
        assertEquals("false", evaluate(eq(literal(42), literal(10))));
        assertEquals("false", evaluate(eq(literal("polop"), literal("pilip"))));
    }

    @Test
    void should_evaluate_not_equalilty() {
        assertEquals("false", evaluate(neq(literal(true), literal(true))));
        assertEquals("false", evaluate(neq(literal(false), literal(false))));
        assertEquals("false", evaluate(neq(literal(42), literal(42))));
        assertEquals("false", evaluate(neq(literal("polop"), literal("polop"))));

        assertEquals("true", evaluate(neq(literal(true), literal(false))));
        assertEquals("true", evaluate(neq(literal(42), literal(10))));
        assertEquals("true", evaluate(neq(literal("polop"), literal("pilip"))));
    }

    @Test
    void should_evaluate_less_or_equal() {
        Property.def("Less or equal evaluation")
                .forAll(integer(), integer())
                .suchThat((x, y) -> areEquals(evaluate(le(literal(x), literal(y))), x <= y))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_less_than() {
        Property.def("Less than evaluation")
                .forAll(integer(), integer())
                .suchThat((x, y) -> areEquals(evaluate(lt(literal(x), literal(y))), x < y))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_greater_or_equal() {
        Property.def("Greater or equal evaluation")
                .forAll(integer(), integer())
                .suchThat((x, y) -> areEquals(evaluate(ge(literal(x), literal(y))), x >= y))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_greater_than() {
        Property.def("Greater than evaluation")
                .forAll(integer(), integer())
                .suchThat((x, y) -> areEquals(evaluate(gt(literal(x), literal(y))), x > y))
                .check()
                .assertIsSatisfied();
    }
}
