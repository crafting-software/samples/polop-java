package sample.polop.interpreter;

import io.vavr.test.Property;
import org.junit.jupiter.api.Test;

import static io.vavr.test.Arbitrary.integer;
import static java.lang.String.valueOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.polop.ast.Expressions.*;
import static sample.polop.interpreter.DSL.evaluate;
import static sample.polop.interpreter.Values.lazyOf;
import static sample.polop.interpreter.Values.value;

class VariableInterpretation {

    @Test
    void should_evaluate_an_undefined_variable() {
        assertEquals("<undefined>", evaluate(variable("unknown")));
    }

    @Test
    void should_evaluate_an_defined_variable() {
        Property.def("Number variable evaluation")
                .forAll(integer())
                .suchThat(value -> evaluate(
                        variable("x"),
                        Frame.empty().set("x", lazyOf(value(value)))).equals(valueOf(value)))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_an_assignation() {
        Property.def("Number variable assignment evaluation")
                .forAll(integer())
                .suchThat(value -> evaluate(sequence(assign("x", literal(value)), variable("x"))).equals(valueOf(value)))
                .check()
                .assertIsSatisfied();
    }
}
