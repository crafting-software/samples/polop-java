package sample.polop.interpreter;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.polop.ast.Expressions.*;
import static sample.polop.interpreter.DSL.evaluate;

class BooleanInterpretation {

    @Test
    void should_evaluate_logical_and() {
        assertEquals("true", evaluate(and(literal(true), literal(true))));
        assertEquals("false", evaluate(and(literal(true), literal(false))));
        assertEquals("false", evaluate(and(literal(false), literal(true))));
        assertEquals("false", evaluate(and(literal(false), literal(false))));
    }

    @Test
    void should_evaluate_logical_or() {
        assertEquals("true", evaluate(or(literal(true), literal(true))));
        assertEquals("true", evaluate(or(literal(true), literal(false))));
        assertEquals("true", evaluate(or(literal(false), literal(true))));
        assertEquals("false", evaluate(or(literal(false), literal(false))));
    }

    @Test
    void should_evaluate_logical_xor() {
        assertEquals("false", evaluate(xor(literal(true), literal(true))));
        assertEquals("true", evaluate(xor(literal(true), literal(false))));
        assertEquals("true", evaluate(xor(literal(false), literal(true))));
        assertEquals("false", evaluate(xor(literal(false), literal(false))));
    }

    @Test
    void should_evaluate_logical_not() {
        assertEquals("false", evaluate(not(literal(true))));
        assertEquals("true", evaluate(not(literal(false))));
    }

    @Test
    void should_evaluate_boolean_cast() {
        assertEquals("true", evaluate(bool(literal(true))));
        assertEquals("false", evaluate(bool(literal(false))));

        assertEquals("true", evaluate(bool(literal(32))));
        assertEquals("false", evaluate(bool(literal(0))));

        assertEquals("true", evaluate(bool(literal("not empty"))));
        assertEquals("false", evaluate(bool(literal(""))));
    }
}
