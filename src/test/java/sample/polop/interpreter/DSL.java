package sample.polop.interpreter;

import sample.polop.ast.Expression;

import static sample.polop.interpreter.Interpreter.interpret;

final class DSL {
    static String evaluate(final Expression expression) {
        return evaluate(expression, Frame.empty());
    }

    static String evaluate(final Expression expression, final Frame context) {
        return interpret(expression, context).repr();
    }

    static boolean areEquals(final String evaluation, final Object expected) {
        return evaluation.equals(expected.toString());
    }
}
