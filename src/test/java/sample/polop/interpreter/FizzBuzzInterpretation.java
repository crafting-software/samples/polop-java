package sample.polop.interpreter;

import org.junit.jupiter.api.Test;
import sample.polop.ast.Expression;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.polop.ast.Expressions.*;
import static sample.polop.interpreter.DSL.evaluate;

class FizzBuzzInterpretation {

    private final static Expression fizzbuzz = functor(
            "v",
            cond(
                    assign(
                            "res",
                            concat(
                                    cond(not(mod(variable("v"), literal(3))), literal("Fizz"), literal("")),
                                    cond(not(mod(variable("v"), literal(5))), literal("Buzz"), literal(""))
                            )),
                    variable("res"),
                    concat(variable("v"))));

    @Test
    void should_evaluate_fizzbuzz() {
        assertEquals("'1'", evaluate(call(fizzbuzz, literal(1))));
        assertEquals("'Fizz'", evaluate(call(fizzbuzz, literal(3))));
        assertEquals("'Buzz'", evaluate(call(fizzbuzz, literal(5))));
        assertEquals("'FizzBuzz'", evaluate(call(fizzbuzz, literal(15))));
    }
}
