package sample.polop.interpreter;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import sample.polop.ast.Expression;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.polop.ast.Expressions.*;
import static sample.polop.interpreter.DSL.evaluate;

class ControlStructureInterpretation {

    @Test
    void should_evaluate_sequence() {
        assertEquals("true", evaluate(sequence(literal(true), literal(false), literal(true))));
        assertEquals("42", evaluate(sequence(literal(true), literal(42))));
    }

    @Test
    void should_evaluate_conditional() {
        assertEquals("'then'", evaluate(cond(literal(true), literal("then"), literal("else"))));
        assertEquals("'else'", evaluate(cond(literal(false), literal("then"), literal("else"))));
    }

    @Test
    void should_evaluate_while() {
        final Expression loop = sequence(
                assign("c", literal(10)),
                while_(
                        gt(variable("c"), literal(0)),
                        assign("c", sub(variable("c"), literal(1))))

        );
        assertEquals("0", evaluate(loop));
    }

    @Test
    @Disabled
    void should_evaluate_for() {
        final Expression loop = sequence(
                assign("res", literal(0)),
                for_("i", literal(0), lt(variable("i"), literal(5)),
                        assign("res", add(variable("res"), variable("i")))));

        assertEquals("10", evaluate(loop));
    }

    @Test
    @Disabled
    void should_evaluate_foreach() {
        final Expression loop = sequence(
                assign("res", literal(0)),
                foreach("i", tuple(literal(1), literal(3), literal(5)),
                        assign("res", add(variable("res"), variable("i")))));

        assertEquals("9", evaluate(loop));
    }


}
