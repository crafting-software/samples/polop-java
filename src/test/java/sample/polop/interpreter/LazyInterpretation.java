package sample.polop.interpreter;

import org.junit.jupiter.api.Test;
import sample.polop.ast.Expression;
import sample.polop.ast.ExpressionVisitor;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static sample.polop.ast.Expressions.*;
import static sample.polop.interpreter.DSL.evaluate;

final class LazyInterpretation {
    private boolean visited = false;

    @Test
    void should_lazy_evaluate_and() {
        markerShouldNotBeEvaluated(and(literal(false), mark(literal(true))));
    }

    @Test
    void should_lazy_evaluate_or() {
        markerShouldNotBeEvaluated(or(literal(true), mark(literal(false))));
    }

    @Test
    void should_lazy_evaluate_call_arguments() {
        markerShouldNotBeEvaluated(call(functor("x", literal(true)), mark(literal(false))));
    }


    private void markerShouldNotBeEvaluated(final Expression expression) {
        evaluate(expression);
        assertFalse(visited);
    }

    private Expression mark(final Expression child) {
        return new Expression() {
            public <T> T accept(final ExpressionVisitor<T> visitor) {
                visited = true;
                return child.accept(visitor);
            }
        };
    }
}
