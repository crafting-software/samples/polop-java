package sample.polop.interpreter;

import io.vavr.test.Arbitrary;
import io.vavr.test.Gen;
import io.vavr.test.Property;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static io.vavr.test.Arbitrary.integer;
import static io.vavr.test.Arbitrary.ofAll;
import static io.vavr.test.Gen.choose;
import static java.math.MathContext.DECIMAL128;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.polop.ast.Expressions.*;
import static sample.polop.interpreter.DSL.areEquals;
import static sample.polop.interpreter.DSL.evaluate;

class ArithmeticInterpretation {

    @Test
    void should_evaluate_addition() {
        Property.def("Addition evaluation")
                .forAll(integer(), integer())
                .suchThat((x, y) -> areEquals(evaluate(add(literal(x), literal(y))), x + y))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_subtraction() {
        Property.def("Subtraction evaluation")
                .forAll(integer(), integer())
                .suchThat((x, y) -> areEquals(evaluate(sub(literal(x), literal(y))), x - y))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_multiplication() {
        Property.def("Multiplication evaluation")
                .forAll(integer(), integer())
                .suchThat((x, y) -> areEquals(evaluate(mul(literal(x), literal(y))), x * y))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_division() {
        Property.def("Division evaluation")
                .forAll(integer().map(BigDecimal::valueOf), integer().filter(i -> i != 0).map(BigDecimal::valueOf))
                .suchThat((x, y) -> areEquals(evaluate(div(literal(x), literal(y))), x.divide(y, DECIMAL128)))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_modulo() {
        Property.def("Modulo evaluation")
                .forAll(integer(), ofAll(choose(1, 50)))
                .suchThat((x, y) -> areEquals(evaluate(mod(literal(x), literal(y))), x % y))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_negate() {
        Property.def("Negate evaluation")
                .forAll(integer())
                .suchThat((x) -> areEquals(evaluate(neg(literal(x))), -x))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_abs() {
        Property.def("Absolute value evaluation")
                .forAll(integer())
                .suchThat((x) -> areEquals(evaluate(abs(literal(x))), Math.abs(x)))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_square() {
        Property.def("Square evaluation")
                .forAll(integer())
                .suchThat((x) -> areEquals(evaluate(sqr(literal(x))), x * x))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_square_root() {
        Property.def("Square root evaluation")
                .forAll(Arbitrary.ofAll(Gen.choose(0.0, 1000000.0)).map(BigDecimal::valueOf))
                .suchThat((x) -> areEquals(evaluate(sqrt(literal(x.multiply(x)))), x))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_evaluate_max() {
        assertEquals("10", evaluate(max(literal(10), literal(3))));
    }

    @Test
    void should_evaluate_min() {
        assertEquals("3", evaluate(min(literal(10), literal(3))));
    }
}
